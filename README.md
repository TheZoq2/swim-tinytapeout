# Swim tinytapeout plugin

A swim plugin for building [tinytapeout](https://tinytapeout.com/) projects

**Currently intended for use with tinytapeout 4**

## Project setup

You need a tinytapeout project as a submodule if your swim project. Fork
[https://github.com/TinyTapeout/tt04-submission-template](https://github.com/TinyTapeout/tt04-submission-template)
and add the resulting repo as a submodule called `tto_repo`

```bash
git submodule add https://github.com/TinyTapeout/tt04-verilog-demo tto_repo
```

Add this plugin to your `swim.toml`

```toml
[plugins.tt]
git = "https://gitlab.com/TheZoq2/swim-tinytapeout"
branch = "main"
args.tapeout_dir="tto_repo"
```

Create your top spade module, it must have a signature that matches the following.
The name should follow the naming scheme specified here [https://tinytapeout.com/hdl/important/](https://tinytapeout.com/hdl/important/)

```spade
#[no_mangle]
entity tt_um_YourUsername_YourProjectName(
    // Dedicated inputs - connected to the input switches
    #[no_mangle] ui_in: int<8>,
    // Dedicated outputs - connected to the 7 segment display
    #[no_mangle] uo_out: &mut int<8>,
    // IOs: Bidirectional Input path
    #[no_mangle] uio_in: int<8>,
    // IOs: Bidirectional Output path
    #[no_mangle] uio_out: &mut int<8>,
    // IOs: Bidirectional Enable path (active high: 0=input, 1=output)
    #[no_mangle] uio_oe: &mut int<8>,
    #[no_mangle] ena: bool,      // will go high when the design is enabled
    #[no_mangle] clk: clock,      // clock
    #[no_mangle] rst_n: bool     // reset_n - low to reset
) {
    // Spade enforces setting outputs, even if we don't care about them right now
    set uo_out = 0;
    set uio_out = 0;
    set uio_oe = 0;
}
```


## Usage

For simulation and testing, run swim as normal.

### Setup

For synthesis, there is a local and the github actions flow. The github actions flow
is what is used by tinytapeout themselves but takes a bit longer to run, so for day-to-day
development, you might prefer the local flow

### Local flow

> The local flow requires some setup, but it is mostly automated. It does
> require docker with permissions to run as your user. If you are not
> comfortable setting that, you can also use podman as a drop-in replacement.
> If you'd like to do that, start podman, then set `export
> DOCKER_HOST=unix://$XDG_RUNTIME_DIR/podman/podman.sock` before running any
> local flow commands

To set up the local flow, run
```
swim plugin tt_setup
```

At this point, we need to work around a small bug. Open `tt_repo/tt/project.py` and comment out the code to `Write commit information`
[https://github.com/TinyTapeout/tt-support-tools/blob/dac3f4a0a4527ca7c65ce44987bdb2eaa25f5525/project.py#L409C35-L417](https://github.com/TinyTapeout/tt-support-tools/blob/dac3f4a0a4527ca7c65ce44987bdb2eaa25f5525/project.py#L409C35-L417)


Then run (You only need to do this once, or after you change `info.toml`)
```
swim plugin update_config
```


To build the project, run
```
swim plugin harden
```

### Github actions flow

Run `swim plugin tt_push` to push the project to your tinytapeout repo and run
the actions. Note that this creates a new commit each time it is run. It is
probably worth adding a note in the readme of that repo that the actual source
code is in your swim project repo



